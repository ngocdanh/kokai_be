const { default: mongoose } = require('mongoose');
const { colorize } = require('../utils/shared');

const MONGODB_HOST = process.env.MONGODB_HOST;
const MONGODB_USER = process.env.MONGODB_USER;
const MONGODB_USER_PASS = process.env.MONGODB_USER_PASS;
const MONGODB_DBNAME = process.env.MONGODB_DBNAME;

const MONGODB_URI = `mongodb+srv://${MONGODB_USER}:${MONGODB_USER_PASS}@${MONGODB_HOST}/${MONGODB_DBNAME}?retryWrites=true&w=majority`;
function connectDB() {
  try {
    const mongooseConnectOptions = {
      autoIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };
    mongoose.connect(MONGODB_URI, mongooseConnectOptions);
    mongoose.connection.on('open', function (ref) {
      console.log(colorize('Connected to mongoDB server.').bgBlue);
      
      // mongoose.connection.db.listCollections().toArray(function (err, names) {
      //     con sole.log(names);
      // });
    });
  }
  catch (errors) {
    console.log(colorize(errors).bgRed);
  }
}

module.exports  = connectDB();