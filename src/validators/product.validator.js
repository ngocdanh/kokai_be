const { body } = require('express-validator');


const productNameValidator = body('name', 'name is string  required and min length 2 character')
  .notEmpty()
  .isLength({ min: 2 });

const slugValidator = body('slug', 'slug is string required and min length 2 character')
  .notEmpty()
  .isLength({ min: 2 });

const priceValidator = body('price', 'price is number required and min value 0')
  .notEmpty()
  .isFloat({ min: 0 });

const salePriceValidator = body('sale_price', 'sale_price is number required and min value 0')
  .notEmpty()
  .isFloat({ min: 0 });

const quantityValidator = body('quantity', 'quantity is number required and min value 0')
  .notEmpty()
  .isFloat({ min: 0 });

const categoriesValidator = body('categories', 'categories is array required and min value 0')
  .notEmpty()
  .isArray();

const createProductValidator = [
  productNameValidator,
  slugValidator,
  priceValidator,
  salePriceValidator,
  quantityValidator,
  categoriesValidator
];
module.exports = {
  createProductValidator
};