const { body } = require('express-validator');


const fistNameValidator = body('fistName', 'fistName is required and minimum/maximum 1-15 characters')
  .notEmpty()
  .isLength({ min: 1, max: 15 });
const lastNameValidator = body('lastName', 'lastName is required and minimum/maximum 1-15 characters')
  .notEmpty()
  .isLength({ min: 1, max: 15 });

const emailValidator = body('email')
  .notEmpty()
  .isLength({ min: 3 })
  .withMessage('email is required and longer than 3 characters')
  .isEmail()
  .withMessage('email not valid');

const passwordValidator = body('password', 'password is required and minimum of 8 characters')
  .isString()
  .isLength({ min: 8 })
  .not()
  .isLowercase()
  .not()
  .isUppercase()
  .not()
  .isNumeric()
  .not()
  .isAlpha();

const registerAccountValidator = [
  fistNameValidator,
  lastNameValidator,
  emailValidator,
  passwordValidator,
];

const loginAccountValidator = [
  emailValidator,
  passwordValidator,
];
module.exports = {
  registerAccountValidator,
  loginAccountValidator
};