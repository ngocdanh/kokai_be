const { body } = require('express-validator');


const productsValidator = body('products', 'product is required and is an (Array)/objectId')
  .notEmpty()
  .isArray()
  .isLength({ min: 1});
const productValidator = body('product', 'product is required and is an (Array)/objectId')
  .notEmpty();
const userObjIdValidator = body('userObjId', 'userObjId is required and is an objectId')
  .notEmpty()
  .isString();

const wishlistValidator = [
  productsValidator,
  userObjIdValidator,
];

const deleteWishlistValidator = [
  productValidator,
  userObjIdValidator,
];

const addWishlistValidator = [
  productValidator,
  userObjIdValidator,
];
module.exports = {
  wishlistValidator,
  deleteWishlistValidator,
  addWishlistValidator
};