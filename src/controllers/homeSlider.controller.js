const { CODES_SUCCESS } = require('../utils/messages');
const { resJsonError, responseError, responseSuccess } = require('../utils/shared');
const { isEmpty } = require('lodash');
const { MODEL } = require('../utils/constants');
const { homeSliderServices } = require('../services');

module.exports = {
  /**
     * signIn
     * @param {any} req request
     * @param {any} res response
     * @param {any} next next
     */
  list: async (req, res)=> {
    try {
      const result = await homeSliderServices.list(req.query);
      if(!isEmpty(result)){
        return res.json(responseSuccess(1401, result));
      }
      return res.json(responseError(4401));
    } catch (err) {
      return resJsonError(res, err);
    }
  },
};