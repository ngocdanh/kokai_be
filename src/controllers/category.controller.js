const { CODES_SUCCESS } = require('../utils/messages');
const { resJsonError, responseError, responseSuccess } = require('../utils/shared');
const productServices = require('../services/product.service');
const { isEmpty } = require('lodash');
const productValidator = require('../validators/product.validator');
const { MODEL } = require('../utils/constants');
const { categoryServices } = require('../services');

module.exports = {
  /**
     * signIn
     * @param {any} req request
     * @param {any} res response
     * @param {any} next next
     */
  list: async (req, res)=> {
    try {
      const result = await categoryServices.list(req.query);
      if(!isEmpty(result)){
        return res.json(responseSuccess(1301, result));
      }
      return res.json(responseError(4301));
    } catch (err) {
      return resJsonError(res, err);
    }
    
  },
  featuredCategory: async (req, res)=> {
    try {
      const result = await categoryServices.featuredCategory(req.query);
      if(!isEmpty(result)){
        return res.json(responseSuccess(1303, result));
      }
      return res.json(responseError(4303));
    } catch (err) {
      return resJsonError(res, err);
    }
    
  },
  detail: async (req, res)=> {
    try {
      const result = await categoryServices.detail(req.query);
      if(!isEmpty(result)){
        return res.json(responseSuccess(1302, result));
      }
      return res.json(responseError(4302));
    } catch (err) {
      return resJsonError(res, err);
    }
    
  }
};