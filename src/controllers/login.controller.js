const { CODES_SUCCESS } = require('../utils/messages');
const { resJsonError, responseError, responseSuccess } = require('../utils/shared');
const { isEmpty, get, random } = require('lodash');
const productValidator = require('../validators/product.validator');
const { MODEL } = require('../utils/constants');
const { productServices, accountServices } = require('../services');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

const refreshTokenKey = process.env.REFRESH_TOKEN_KEY;

module.exports = {
  /**
      * create account
      * @param {any} req request
      * @param {any} res response
      * @param {any} next next
      */
  register: async (req, res) => {
    try {
      /* Check express validator */
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.json(responseError(4501, errors.errors));
      }

      /* Find email if exists  */
      const email = get(req, 'body.email', '');
      const findByEmailUser = await accountServices.findByEmail({email});
      if(!isEmpty(findByEmailUser)) {
        return res.json(responseError(4551));
      }

      /* Create account */
      const result = await accountServices.create(req);
      if (!isEmpty(result)) {
        return res.json(responseSuccess(1501, result));
      }

      return res.json(responseError(4501));
    } catch (err) {
      return resJsonError(res, err);
    }
  },
  login: async (req, res) => {
    try {
      /* Check express validator */
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.json(responseError(4501, errors.errors));
      }

      const result = await accountServices.loginWithEmail(req);
      if (!isEmpty(result)) {
        if(result?.isSuccess === false) {
          if(result?.error === 'email') {
            return res.json(responseError(4556));
          } else if(result?.error === 'password') {
            return res.json(responseError(4555));
          }
        } else {
          return res.json(responseSuccess(1502, result));
        }
      }

      return res.json(responseError(4502));
    } catch (err) {
      return resJsonError(res, err);
    }
  },
  refreshToken: async (req, res) => {
    try {
      /* Check express validator */
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.json(responseError(4504, errors.errors));
      }
      const refreshToken = get(req, 'body.refreshToken', '');
      jwt.verify(refreshToken, refreshTokenKey, async (err, decoded) => {
        if (err) {
          return res.json(responseError(4557));
        }
        req.decoded = {
          ...decoded,
        };
      });
      const result = await accountServices.refreshToken(req);
      if (result.isSuccess) {
        return res.json(responseSuccess(1504, result));
      }
      return res.json(responseError(4504));
    } catch (err) {
      return resJsonError(res, err);
    }
  },
};