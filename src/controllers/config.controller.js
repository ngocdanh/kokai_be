const { CODES_SUCCESS } = require('../utils/messages');
const { resJsonError, responseError, responseSuccess } = require('../utils/shared');
const { isEmpty } = require('lodash');
const { MODEL } = require('../utils/constants');
const { configServices } = require('../services');

module.exports = {
  /**
     * signIn
     * @param {any} req request
     * @param {any} res response
     * @param {any} next next
     */
  menuList: async (req, res)=> {
    try {
      const result = await configServices.list(req.query);
      if(!isEmpty(result)){
        let statusCode = 1201;
        if(req.query.name === 'menu-header') {
          statusCode = 1202;
        } else if(req.query.name === 'menu-footer') {
          statusCode = 1203;
        } else if(req.query.name === 'menu-category') {
          statusCode = 1204;
        }
        return res.json(responseSuccess(statusCode, result));
      }
      return res.json(responseError(4201));
    } catch (err) {
      return resJsonError(res, err);
    }
  },
};