const { CODES_SUCCESS } = require('../utils/messages');
const { resJsonError, responseError, responseSuccess } = require('../utils/shared');
const { isEmpty, get, random } = require('lodash');
const productValidator = require('../validators/product.validator');
const { MODEL } = require('../utils/constants');
const { productServices, accountServices } = require('../services');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

module.exports = {
  /**
      * verify token
      * @param {any} req request
      * @param {any} res response
      * @param {any} next next
      */
  verifyToken: async (req, res, next) => {
    try {
      const accessToken = req.headers['x-access-token'] || req.body.accessToken || req.query.accessToken;
      if (!accessToken) {
        return res.json(responseError(4553));
      }
      jwt.verify(accessToken, process.env.TOKEN_KEY, async (err, decoded) => {
        if (err) {
          return res.json(responseError(4554));
        }
        req.decoded = {
          ...decoded,
        };
        return next();
      });
    } catch (err) {
      if (err.name === 'JsonWebTokenError') {
        return res.json(responseError(4554));
      }
      return resJsonError(res, err);
    }
  },
};