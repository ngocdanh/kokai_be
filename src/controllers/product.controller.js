const { CODES_SUCCESS } = require('../utils/messages');
const { resJsonError, responseError, responseSuccess } = require('../utils/shared');
const { isEmpty, get, random } = require('lodash');
const productValidator = require('../validators/product.validator');
const { MODEL } = require('../utils/constants');
const { productServices } = require('../services');
const { validationResult } = require('express-validator');

module.exports = {
  /**
      * create Product
      * @param {any} req request
      * @param {any} res response
      * @param {any} next next
      */
  create: async (req, res) => {
    try {
      /* Check express validator */
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.json(responseError(4101, errors.errors));
      }

      /* Find product double slug */
      const findProduct = await productServices.findBySlug(req.body);
      if(!isEmpty(findProduct)) {
        return res.json(responseError(4101));
      }

      /* Create product */
      const result = await productServices.create(req);
      if (!isEmpty(result)) {
        return res.json(responseSuccess(1101, result));
      }

      return res.json(responseError(4101));
    } catch (err) {
      return resJsonError(res, err);
    }
  },
  list: async (req, res) => {
    try {
      const result = await productServices.list(req.query);
      if (!isEmpty(result)) {
        return res.json(responseSuccess(1102, result));
      }
      return res.json(responseError(4102));
    } catch (err) {
      return resJsonError(res, err);
    }

  },
  listProductPopular: async (req, res) => {
    try {
      const result = await productServices.listProductPopular(req.query);
      if (!isEmpty(result)) {
        return res.json(responseSuccess(1105, result));
      }
      return res.json(responseError(4105));
    } catch (err) {
      return resJsonError(res, err);
    }

  },
  listProductBestSeller: async (req, res) => {
    try {
      const result = await productServices.listProductBestSeller(req.query);
      if (!isEmpty(result)) {
        return res.json(responseSuccess(1106, result));
      }
      return res.json(responseError(4106));
    } catch (err) {
      return resJsonError(res, err);
    }

  },
  listDealOfDay: async (req, res) => {
    try {
      const result = await productServices.listDealOfDay(req.query);
      if (!isEmpty(result)) {
        return res.json(responseSuccess(1107, result));
      }
      return res.json(responseError(4107));
    } catch (err) {
      return resJsonError(res, err);
    }

  },
  detail: async (req, res) => {
    try {
      const result = await productServices.findBySlug(req.query);
      if (!isEmpty(result)) {
        return res.json(responseSuccess(1103, result));
      }
      return res.json(responseError(4103));
    } catch (err) {
      return resJsonError(res, err);
    }

  },
  updateAll: async (req, res) => {
    try {
      const result = await productServices.updateAll(req.query);
      if (!isEmpty(result)) {
        return res.json(responseSuccess(1104, result));
      }
      return res.json(responseError(4104));
    } catch (err) {
      return resJsonError(res, err);
    }

  }
};