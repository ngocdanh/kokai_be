const { resJsonError, responseError, responseSuccess } = require('../utils/shared');
const { isEmpty, get, random } = require('lodash');
const productValidator = require('../validators/product.validator');
const { MODEL } = require('../utils/constants');
const { productServices, accountServices, wishlistServices } = require('../services');
const { validationResult } = require('express-validator');

module.exports = {
  /**
      * create wishlist
      * @param {any} req request
      * @param {any} res response
      * @param {any} next next
      */
  update: async (req, res) => {
    try {
      /* Check express validator */
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.json(responseError(4601, errors.errors));
      }

      /* Find objId if exists  */
      const userObjId = get(req, 'body.userObjId', null);
      const findByUserObjId= await wishlistServices.findByUserObjId({userObjId});
      if(!isEmpty(findByUserObjId)) {
        const _id = get(findByUserObjId, '_id', null);
        /* update wishlist */
        const result = await wishlistServices.update(req, _id);
        if (!isEmpty(result)) {
          return res.json(responseSuccess(1602, result));
        }
        return res.json(responseError(4602));
      } else {
        /* Create wishtlist */
        const result = await wishlistServices.create(req);
        if (!isEmpty(result)) {
          return res.json(responseSuccess(1601, result));
        }
        return res.json(responseError(4601));
      }
    } catch (err) {
      return resJsonError(res, err);
    }
  },
  remove: async (req, res) => {
    try {
      /* Check express validator */
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.json(responseError(4601, errors.errors));
      }

      /* Find objId if exists  */
      const userObjId = get(req, 'body.userObjId', null);
      const findByUserObjId= await wishlistServices.findByUserObjId({userObjId});
      if(!isEmpty(findByUserObjId)) {
        /* delete item wishlist */
        const result = await wishlistServices.removeProduct(req);
        if (!isEmpty(result)) {
          return res.json(responseSuccess(1605, result));
        }
        return res.json(responseError(4605));
      } else {
        return res.json(responseError(4605));
      }
    } catch (err) {
      return resJsonError(res, err);
    }
  },
  add: async (req, res) => {
    try {
      /* Check express validator */
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.json(responseError(4601, errors.errors));
      }

      /* Find objId if exists  */
      const userObjId = get(req, 'body.userObjId', null);
      const findByUserObjId= await wishlistServices.findByUserObjId({userObjId});
      if(!isEmpty(findByUserObjId)) {
        /* add item wishlist */
        const result = await wishlistServices.addProduct(req);
        if (!isEmpty(result)) {
          return res.json(responseSuccess(1606, result));
        }
        return res.json(responseError(4606));
      } else {
        return res.json(responseError(4606));
      }
    } catch (err) {
      return resJsonError(res, err);
    }
  },
  detail: async (req, res) => {
    try {
      const userObjId = get(req, 'query.userObjId', null);
      const result = await wishlistServices.findByUserObjId({userObjId});
      if (!isEmpty(result)) {
        return res.json(responseSuccess(1603, result));
      }
      return res.json(responseError(4603));
    } catch (err) {
      return resJsonError(res, err);
    }

  },
  list: async (req, res) => {
    try {
      const result = await wishlistServices.list(req.query);
      if (!isEmpty(result)) {
        return res.json(responseSuccess(1604, result));
      }
      return res.json(responseError(4604));
    } catch (err) {
      return resJsonError(res, err);
    }

  },
};