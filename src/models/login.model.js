const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');
const { MODEL } = require('../utils/constants');
const { commonModel } = require('../utils/shared');

const {ObjectId } = mongoose.Types;

const loginBase = {
  info: {
    fistName: { type: String,trim: true, required: true },
    lastName: {type: String,trim: true, required: true },
    fullName: {type: String,trim: true, required: true },
    phone: { type: String, trim: true },
    address: { type: String, trim: true },
    role: { type: String, trim: true },
  },
  email: { type: String,trim: true, required: true},
  password: { type: String, trim: true, required: true },
};

const login = {...loginBase, ...commonModel};

const loginSchema = new Schema(login, {versionKey: false, strict: false, toJSON: {virtuals: true}});
loginSchema.plugin(mongoosePaginate);

const loginModel = mongoose.model(MODEL.accounts, loginSchema);
module.exports = loginModel;  