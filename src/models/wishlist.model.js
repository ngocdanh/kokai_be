const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');
const { MODEL } = require('../utils/constants');
const { commonModel } = require('../utils/shared');

const {ObjectId } = mongoose.Types;

const wishlistBase = {
  userObjId: { type: ObjectId, required: true, },
  products: [{type: ObjectId, ref: MODEL.products, required: true}],
};

const wishlists = {...wishlistBase, ...commonModel};

const wishlistSchema = new Schema(wishlists, {versionKey: false, toJSON: {virtuals: true}});
wishlistSchema.plugin(mongoosePaginate);

const wishlistModel = mongoose.model(MODEL.wishlist, wishlistSchema);
module.exports = wishlistModel;  