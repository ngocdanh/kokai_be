const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');
const { MODEL } = require('../utils/constants');
const { commonModel } = require('../utils/shared');

const { ObjectId } = mongoose.Types;

const categoryBase = {
  name: { type: Object, trim: true, required: true, },
  slug: { type: String, trim: true, required: true, },
  id: { type: Number, trim: true, required: true, },
  img: { type: Array, trim: true, required: true, },
  bg: { type: String, trim: true, required: true, },
  amount: { type: Number, trim: true, required: true, },
};

const categories = { ...categoryBase, ...commonModel };

const categorySchema = new Schema(categories, { versionKey: false });
categorySchema.plugin(mongoosePaginate);

const categoryModel = mongoose.model(MODEL.categories, categorySchema);
module.exports = categoryModel;  