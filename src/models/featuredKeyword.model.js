const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');
const { MODEL } = require('../utils/constants');
const { commonModel } = require('../utils/shared');

const {ObjectId } = mongoose.Types;

const featuredKeywordBase = {};

const featuredKeywords = {...featuredKeywordBase, ...commonModel};

const featuredKeyWordSchema = new Schema(featuredKeywords, {versionKey: false});
featuredKeyWordSchema.plugin(mongoosePaginate);

const featuredKeywordModel = mongoose.model(MODEL.featuredKeywords, featuredKeyWordSchema);
module.exports = featuredKeywordModel;  