const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const { MODEL } = require('../utils/constants');
const { commonModel } = require('../utils/shared');
const {ObjectId } = mongoose.Types;
const mongoosePaginate = require('mongoose-paginate-v2');

const configBase = {
  title: { type: Object,trim: true,required: true, },
  background: {type: String,trim: true, required: true, },
};

const homeSlider = {...configBase, ...commonModel};

const homeSliderSchema = new Schema(homeSlider, {versionKey: false});
homeSliderSchema.plugin(mongoosePaginate);

const homeSliderModel = mongoose.model(MODEL.homeSlider, homeSliderSchema);
module.exports = homeSliderModel;  