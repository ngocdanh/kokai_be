const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');
const { MODEL } = require('../utils/constants');
const { commonModel } = require('../utils/shared');
const {ObjectId } = mongoose.Types;

const configBase = {
  id: { type: String,trim: true,required: true, },
  title: {type: Object,trim: true, required: true, },
  icon: { type: String,trim: true,required: true, },
  url: { type: String,trim: true,required: true, },
  name: { type: String,trim: true,required: true, },
};

const configs = {...configBase, ...commonModel};

const menuSchema = new Schema(configs, {versionKey: false});

const menuModel = mongoose.model(MODEL.configs, menuSchema);
module.exports = menuModel;  