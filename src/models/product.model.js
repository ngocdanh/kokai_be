const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');
const { MODEL } = require('../utils/constants');
const { commonModel } = require('../utils/shared');

const {ObjectId } = mongoose.Types;

const productBase = {
  id: { type: Number,trim: true,required: true, },
  name: { type: String,trim: true,required: true, },
  slug: {type: String,trim: true, required: true, },
  description: { type: String,trim: true, },
  price: { type: Number, trim: true, required: true, },
  sale_price: { type: Number, trim: true, required: true, },
  sku: { type: String, trim: true},
  quantity: { type: Number, trim: true, required: true, },
  variations: { type: Array, trim: true },
  product_type: { type: String, trim: true },
  unit: { type: String, trim: true, required: true, },
  image: { type: Object, trim: true, required: true, },
  gallery: { type: Array, trim: true, required: true, },
  max_price: { type: Number, trim: true },
  min_price: { type: Number, trim: true },
  categories: { type: Array, trim: true, required: true, }
};

const products = {...productBase, ...commonModel};

const productSchema = new Schema(products, {versionKey: false, strict: false, toJSON: {virtuals: true}});
productSchema.plugin(mongoosePaginate);

const productModel = mongoose.model(MODEL.products, productSchema);
module.exports = productModel;  