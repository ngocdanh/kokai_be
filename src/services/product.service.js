const { get, trim, random, isEmpty, round } = require('lodash');
const mongoose = require('mongoose');
const productModel = require('../models/product.model');
const { PROMOTION_CODES, myCustomLabels } = require('../utils/constants');
const {
  promiseReject,
  regExpSearch,
  promiseResolve,
  convertToObjectId,
  resJsonError
} = require('../utils/shared');

const fields = {
  _id: 1,
  name: 1,
  slug: 1,
  'categories.name': 1,
  'categories.slug': 1,
  'categories.icon': 1,
  'categories.parent': 1,
  price: 1,
  sale_price: 1,
  quantity: 1,
  'image.original': 1,
  'image.thumbnail': 1,
  vote: 1,
  discount: 1,
  total: 1,
  seller_rank: 1,
  sold: 1,
  created_at: 1,
  promotion_code: 1,
};

const create = async (data) => {
  try {
    const set = {};
    set.name = get(data, 'body.name', '');
    set.slug = get(data, 'body.slug', '');
    set.description = get(data, 'body.description', '');
    set.price = get(data, 'body.price', 0);
    set.sale_price = set.price - random(0, set.price - 10);
    set.sale_price = get(data, 'body.sale_price', 0);
    set.sku = get(data, 'body.sku', '');
    set.quantity = get(data, 'body.quantity', 0);
    set.variations = get(data, 'body.variations', null);
    set.product_type = get(data, 'body.product_type', '');
    set.unit = get(data, 'body.unit', '');
    set.image = get(data, 'body.image', null);
    set.gallery = get(data, 'body.gallery', null);
    set.max_price = get(data, 'body.max_price', 0);
    set.min_price = get(data, 'body.min_price', 0);
    set.categories = get(data, 'body.categories', null);
    const result = await productModel.create(set);
    return promiseResolve(result);
  } catch (errors) {
    return promiseReject(errors);
  }
};


const list = async (query) => {
  try {
    const conditions = {};
    const categories = get(query, 'categories', []);
    const category = get(query, 'category', []);
    const page = get(query, 'page', 1);
    const limit = get(query, 'limit', 10);
    const search = get(query, 'search', '');
    let sortKey = get(query, 'sortKey', 'created_at'); 
    const sortOrder = get(query, 'sortOrder', -1);
    const type = get(query, 'type', []);
    if (!isEmpty(search)) {
      const regex = regExpSearch(search);
      conditions.$or = [
        { name: regex }
      ];
    }
    if (!isEmpty(categories)) {
      conditions.$and = categories.map(category => ({
        categories: {
          $elemMatch: { slug: category }
        }
      }));
    }

    if (!isEmpty(category)) {
      conditions.categories = {
        $elemMatch: { slug: category }
      };
    }

    if(query.sortKey) {
      switch(query.sortKey) {
      case 'featured': 
        sortKey = 'view';
        break;
      case 'selling': 
        sortKey = 'seller_rank';
        break;
      case 'trending': 
        sortKey = 'view';
        break;
      case 'rated': 
        sortKey = 'vote';
        break;
      case 'recently': 
        sortKey = 'updated_at';
        break;
      }
    }

    const options = {
      sort: {
        [sortKey]: sortOrder,
      },
      page: page,
      limit: limit,
      // lean: true,
      customLabels: myCustomLabels,
      select: fields,
    };
    let result = [];
    if (!isEmpty(type)) {
      const promises = [];
      for(let i =0; i<type.length; i++){
        conditions.sort = {
          [type[i]]: -1,
        };
        const data = await productModel.paginate(conditions, options);
        promises.push(data);
      }
      result = await Promise.all(promises).then(data => data);
    } else {
      result = await productModel.paginate(conditions, options);
    }
    return promiseResolve(result);
  } catch (err) {
    return promiseReject(err);
  }
};

const listProductPopular = async (query) => {
  try {
    const conditions = {};
    const sortKey = get(query, 'sortKey', 'view');
    const sortOrder = get(query, 'sortOrder', -1);
    const page = get(query, 'page', 1);
    const limit = get(query, 'limit', 10);

    const options = {
      sort: {
        [sortKey]: sortOrder,
      },
      page: page,
      limit: limit,
      // lean: true,
      customLabels: myCustomLabels,
      select: fields,
    };
    const result = await productModel.paginate(conditions, options);
    return promiseResolve(result);
  } catch (err) {
    return promiseReject(err);
  }
};
const listProductBestSeller = async (query) => {
  try {
    const conditions = {};
    const sortKey = get(query, 'sortKey', 'seller_rank');
    const sortOrder = get(query, 'sortOrder', -1);
    const page = get(query, 'page', 1);
    const limit = get(query, 'limit', 4);


    const options = {
      sort: {
        [sortKey]: sortOrder,
      },
      page: page,
      limit: limit,
      // lean: true,
      customLabels: myCustomLabels,
      select: fields,
    };
    const result = await productModel.paginate(conditions, options);
    return promiseResolve(result);
  } catch (err) {
    return promiseReject(err);
  }
};

const listDealOfDay = async (query) => {
  try {
    const conditions = {};
    const page = get(query, 'page', 1);
    const limit = get(query, 'limit', 4);
    const type = get(query, 'limit', 4);
    const search = get(query, 'search', '');
    const sortKey = get(query, 'sortKey', 'promotion_code');
    const sortOrder = get(query, 'sortOrder', -1);
    conditions.promotion_code = 'sale_of_day';
    if (search) {
      const regex = regExpSearch(search);
      conditions.$or = [
        { name: regex }
      ];
    }

    const options = {
      sort: {
        [sortKey]: sortOrder,
      },
      page: page,
      limit: limit,
      // lean: true,
      customLabels: myCustomLabels,
      select: fields,
    };

    const result = await productModel.paginate(conditions, options);
    return promiseResolve(result);
  } catch (err) {
    return promiseReject(err);
  }
};

const updateAll = async (query) => {
  try {
    /* Random update all */
    const result = await productModel.updateMany(
      {
        vote: { $exists: false },
        purchase: { $exists: false },
        view: { $exists: false },
        discount: { $exists: false },
        total: { $exists: false },
        seller_rank: { $exists: false },
        sold: { $exists: false },
        promotion_code: { $exists: false },
      },
      [{
        $set: {
          // vote: {$sum: [{$round: [{$multiply: [{ $rand: {} }, 2] }, 2]}, 3]},
          // purchase: {$round: [{$multiply: [{ $rand: {} }, 1000] }, 0]},
          // view: {$round: [{$multiply: [{ $rand: {} }, 10000] }, 0]},
          // discount:{$multiply: [{$round: [{$subtract: [1, {$divide: ['$sale_price', '$price']}]}, 2]}, 100]},
          // total: {$sum: ['$purchase', {$round: [{$multiply: [{ $rand: {} }, 300] }, 0]}]},
          // seller_rank: {$abs: {$round: [ {$multiply: [ {$subtract: ['$total', '$purchase']}, 100]}, 0]}},
          // sold: {$subtract: ['$total', '$purchase']},
          // promotion_code: null,
          // in_stock: {$round: [{$multiply: [{ $rand: {} }, 1] }, 0]}
        }
      }]
    );
    return promiseResolve(result);
  } catch (errors) {
    return promiseReject(errors);
  }
};

const findById = async (query) => {
  try {
    const conditions = {};
    if (query.productObjId) {
      conditions._id = convertToObjectId(query.productObjId);
    }
    const result = await productModel.findOne(conditions);
    return promiseResolve(result);
  } catch (errors) {
    return promiseReject(errors);
  }
};

const findBySlug = async (query) => {
  try {
    const conditions = {};
    conditions.slug = get(query, 'slug', '');
    const result = await productModel.findOne(conditions);
    return promiseResolve(result);
  } catch (errors) {
    return promiseReject(errors);
  }
};

module.exports = {
  create,
  list,
  findById,
  findBySlug,
  updateAll,
  listProductPopular,
  listProductBestSeller,
  listDealOfDay,
};