const { get, trim, random, isEmpty, round, isArray } = require('lodash');
const mongoose = require('mongoose');
const loginModel = require('../models/login.model');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const {  MODEL, myCustomLabels } = require('../utils/constants');
const {
  promiseReject,
  promiseResolve,
  generateHashPassword,
  generatorTime,
  convertStringToUTF8,
  generatorKey,
  convertToObjectId,
  populateModel
} = require('../utils/shared');
const wishlistModel = require('../models/wishlist.model');

const populate = [
  populateModel('products', '_id name slug price sale_price in_stock image vote')
];

const create = async (data) => {
  try {
    const set = {};
    const userObjId = get(data, 'body.userObjId', null);
    set.userObjId = convertToObjectId(userObjId);
    set.products = get(data, 'body.products', []);
    
    const result = await wishlistModel.create(set);
    return promiseResolve(result);
  } catch (errors) {
    return promiseReject(errors);
  }
};

const update = async (data, _id) => {
  try {
    const conditions = {
      _id
    };
    const set = {};
    const userObjId = get(data, 'body.userObjId', null);
    set.userObjId = convertToObjectId(userObjId);
    set.products = get(data, 'body.products', []);
    set.updatedAt = generatorTime();

    const result = await wishlistModel
      .findOneAndUpdate(conditions, set, {new: true})
      .populate(populate);
    
    return promiseResolve(result);
  } catch (errors) {
    return promiseReject(errors);
  }
};

const removeProduct = async (data) => {
  try {
    const userObjId = get(data, 'body.userObjId', null);
    const conditions = {};
    const set = {};
    conditions.userObjId = userObjId;
    const product = get(data, 'body.product', null);
    if(isArray(product)) {
      set.$pullAll = {products: product};
    } else {
      set.$pullAll = {products: [product]};
    }
    set.updatedAt = generatorTime();
    const result = await wishlistModel
      .findOneAndUpdate(conditions, set, {new: true})
      .populate(populate);
    return promiseResolve(result);
  } catch (errors) {
    return promiseReject(errors);
  }
};

const addProduct = async (data) => {
  try {
    const userObjId = get(data, 'body.userObjId', null);
    const conditions = {};
    const set = {};
    conditions.userObjId = userObjId;
    const product = get(data, 'body.product', null);
    if(isArray(product)) {
      set.$push = {products: product};
    } else {
      set.$push = {products: [product]};
    }
    set.updatedAt = generatorTime();
    const result = await wishlistModel
      .findOneAndUpdate(conditions, set, {new: true})
      .populate(populate);
    return promiseResolve(result);
  } catch (errors) {
    return promiseReject(errors);
  }
};

const findByUserObjId = async (data) => {
  try {
    const conditions = {};
    const userObjId = get(data, 'userObjId', null);
    conditions.userObjId = convertToObjectId(userObjId);
    const result = await wishlistModel
      .findOne(conditions)
      .populate(populate);
    
    return promiseResolve(result);
  } catch (errors) {
    return promiseReject(errors);
  }
};

const list = async (query) => {
  try {
    const conditions = {};
    const sortKey = get(query, 'sortKey', 'createdAt');
    const sortOrder = get(query, 'sortOrder', -1);
    const page = get(query, 'page', 1);
    const limit = get(query, 'limit', 10);


    const options = {
      sort: {
        [sortKey]: sortOrder,
      },
      page: page,
      populate: populate,
      limit: limit,
      lean: true,
      customLabels: myCustomLabels,
    };
    const result = await wishlistModel.paginate(conditions, options);
    return promiseResolve(result);
  } catch (err) {
    return promiseReject(err);
  }
};

module.exports = {
  create,
  findByUserObjId,
  list,
  update,
  removeProduct,
  addProduct,
};