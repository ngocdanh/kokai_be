const { get, trim } = require('lodash');
const mongoose = require('mongoose');
const categoryModel = require('../models/category.model');
const productModel = require('../models/product.model');
const { MODEL, myCustomLabels } = require('../utils/constants');
const { promiseReject, regExpSearch, promiseResolve, convertToObjectId } = require('../utils/shared');

const list = async (query) => {
  try {
    const conditions = {};
    const categories = get(query, 'categories', []);
    const page = get(query, 'page', 1);
    const limit = get(query, 'limit', 10);
    const search = get(query, 'search', '');
    const sortKey = get(query, 'sortKey', 'createdAt');
    const sortOrder = get(query, 'sortOrder', -1);

    if (search) {
      const regex = regExpSearch(search);
      conditions.$or = [
        { name: regex }
      ];
    }
    const options = {
      sort: {
        [sortKey]: sortOrder,
      },
      page: page,
      limit: limit,
      // lean: true,
      customLabels: myCustomLabels,
    };
    const result = await categoryModel.paginate(conditions, options);
    return promiseResolve(result);
  } catch (err) {
    return promiseReject(err);
  }
};

const featuredCategory = async (query) => {
  try {
    const conditions = {};
    const page = get(query, 'page', 1);
    const limit = get(query, 'limit', 10);
    const sortKey = get(query, 'sortKey', 'createdAt');
    const sortOrder = get(query, 'sortOrder', -1);
      
    const options = {
      sort: {
        ['icon']: sortOrder,
      },
      page: page,
      limit: limit,
      customLabels: myCustomLabels,
    };
    const result = await categoryModel.paginate(conditions, options);
    return promiseResolve(result);
  } catch (err) {
    return promiseReject(err);
  }
};

const detail = async (query) => {
  try {
    const conditions = {};
    if (query.categoryObjId) {
      conditions._id = convertToObjectId(query.categoryObjId);
    }
    if (query.slug) {
      conditions.slug = query.slug;
    }
    const result = await categoryModel.findOne(conditions);
    return promiseResolve(result);
  } catch (errors) {
    return resJsonError(res, errors, modelName);
  }
};

module.exports = {
  list,
  featuredCategory,
  detail,
};