const { get, trim } = require('lodash');
const mongoose = require('mongoose');
const homeSliderModel = require('../models/homeSlider.model');
const { MODEL, myCustomLabels } = require('../utils/constants');
const { promiseReject, promiseResolve } = require('../utils/shared');

const  list = async (query, res) => {
  try {
    const conditions = {};
    const page = get(query, 'page', 1);
    const limit = get(query, 'limit', 10);
    const sortKey = get(query, 'sortKey', 'createdAt');
    const sortOrder = get(query, 'sortOrder', -1);
  
    const options = {
      sort: {
        [sortKey]: sortOrder,
      },
      page: page,
      limit: limit,
      customLabels: myCustomLabels,
    };
    const result = await homeSliderModel.paginate(conditions, options);
    return promiseResolve(result);
  } catch (errors) {
    return promiseReject(errors);
  }
};

module.exports = {
  list,
};