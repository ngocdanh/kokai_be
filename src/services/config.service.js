const { get, trim } = require('lodash');
const mongoose = require('mongoose');
const configModel = require('../models/config.model');
const productModel = require('../models/product.model');
const { MODEL } = require('../utils/constants');
const { promiseReject, regExpSearch, promiseResolve, convertToObjectId } = require('../utils/shared');

const  list = async (query) => {
  try {
    const conditions = {};
    if(query.name) {
      conditions.name = query.name;
    }
    const result = await configModel.find(conditions).sort({id: 1});
    return promiseResolve(result);
  } catch (errors) {
    return resJsonError(res, errors, modelName);
  }
};

module.exports = {
  list,
};