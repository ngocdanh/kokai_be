
module.exports = {
  productServices: require('./product.service'),
  configServices: require('./config.service'),
  categoryServices: require('./category.service'),
  homeSliderServices: require('./homeSlider.service'),
  accountServices: require('./account.service'),
  wishlistServices: require('./wishlist.service'),
};