const { get, trim, random, isEmpty, round } = require('lodash');
const mongoose = require('mongoose');
const loginModel = require('../models/login.model');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { ACCOUNT_TYPE, EXPIRE_TIME_TOKEN, EXPIRE_TIME_REFRESH_TOKEN } = require('../utils/constants');
const {
  promiseReject,
  promiseResolve,
  generateHashPassword,
  generatorTime,
  convertStringToUTF8,
  generatorKey
} = require('../utils/shared');
const refreshTokenKey = process.env.REFRESH_TOKEN_KEY;
const secretKey = process.env.TOKEN_KEY;

const create = async (data) => {
  try {
    const fistName = convertStringToUTF8(get(data, 'body.fistName', ''));
    const lastName = convertStringToUTF8(get(data, 'body.lastName', ''));
    const password = get(data, 'body.password', '');
    const phone = get(data, 'body.phone', '');
    
    const set = {};
    set.info = {};
    set.info.fistName = fistName;
    set.info.lastName = lastName;
    if(fistName && lastName) {
      set.info.fullName = `${fistName} ${lastName}`;
    }
    set.info.address = null;
    set.info.phone = phone;
    set.password = generateHashPassword(password);
    set.email = get(data, 'body.email', '');
    set.role = ACCOUNT_TYPE.user;
    const resultUser = await loginModel.create(set);
    if(!isEmpty(resultUser)) {
      const response = {};
      response.userObjId = resultUser._id;
      response.createdAt = resultUser.createdAt;
      response.info = get(resultUser, 'info', {});
      
      response.accessToken = jwt.sign(
        { 
          _id: resultUser._id, 
          email: resultUser.email
        },
        secretKey,
        {
          expiresIn: EXPIRE_TIME_TOKEN,
        }
      );
      response.refreshToken = jwt.sign(
        { 
          _id: resultUser._id, 
          email: resultUser.email
        },
        refreshTokenKey,
        {
          expiresIn: EXPIRE_TIME_REFRESH_TOKEN,
        }
      );
      return promiseResolve(response);
    }
  } catch (errors) {
    return promiseReject(errors);
  }
};

const findByEmail = async (data) => {
  try {
    const conditions = {};
    conditions.email = get(data, 'email', '');
    conditions.role = ACCOUNT_TYPE.user;

    const result = await loginModel.findOne(conditions);
    return promiseResolve(result);
  } catch (errors) {
    return promiseReject(errors);
  }
};

const loginWithEmail = async (data) => {
  try {
    const conditions = {};
    const password = get(data, 'body.password', '');
    conditions.email = get(data, 'body.email', '');
    const resultUser = await loginModel.findOne(conditions);
    const result = {};
    if(!resultUser) {
      result.error = 'email';
      result.isSuccess = false;
    } else {
      const checkUser = bcrypt.compareSync(password, resultUser?.password);
      if(!checkUser) {
        result.error = 'password';
        result.isSuccess = false;
      } else {
        result.isSuccess = true;
        result.info=  resultUser?.info;
        result.userObjId= resultUser?._id;
        result.email= resultUser?.email;
        result.role= resultUser?.role;
        result.createdAt= resultUser?.createdAt;

        const sceretKey = process.env.TOKEN_KEY;
        result.accessToken = jwt.sign(
          { 
            _id: resultUser._id, 
            email: resultUser.email
          },
          sceretKey,
          {
            expiresIn: EXPIRE_TIME_TOKEN,
          }
        );
        result.refreshToken = jwt.sign(
          { 
            _id: resultUser._id, 
            email: resultUser.email
          },
          refreshTokenKey,
          {
            expiresIn: EXPIRE_TIME_REFRESH_TOKEN,
          }
        );
      }
    }
    
    return promiseResolve(result);
  } catch (errors) {
    return promiseReject(errors);
  }
};


const refreshToken = async (data) => {
  try {
    const conditions = {};
    conditions.email = get(data, 'body.email', '');
    conditions._id = get(data, 'body.userObjId', '');

    const resultUser = await loginModel.findOne(conditions);
    const result = {};
    
    if(!isEmpty(resultUser)) {
      result.accessToken = jwt.sign(
        { 
          _id: resultUser._id, 
          email: resultUser.email
        },
        secretKey,
        {
          expiresIn: EXPIRE_TIME_TOKEN,
        }
      );
      result.isSuccess = true; 
      result.userObjId= resultUser?._id;
      result.email= resultUser?.email;
    } else {
      result.isSuccess = false;
    }
    return promiseResolve(result);
  } catch (errors) {
    return promiseReject(errors);
  }
};


module.exports = {
  create,
  findByEmail,
  loginWithEmail,
  refreshToken,
};