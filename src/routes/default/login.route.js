const loginController = require('../../controllers/login.controller');
const { registerAccountValidator, loginAccountValidator } = require('../../validators/login.validator');

module.exports = (apiRoute) => {
  apiRoute.route('/register').post(registerAccountValidator, loginController.register);
  apiRoute.route('/login').post(loginAccountValidator, loginController.login);
  apiRoute.route('/refresh-token').post(loginController.refreshToken);

};