module.exports = (app)=> {
  require('./login.route')(app);
  require('./product.route')(app);
  require('./config.route')(app);
  require('./category.route')(app);
  require('./homeSlider.route')(app);
};