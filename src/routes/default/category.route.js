const categoryController = require('../../controllers/category.controller');

module.exports = (apiRoute) => {
  apiRoute.route('/categories/list').get(categoryController.list);
  apiRoute.route('/categories/featured-categories').get(categoryController.featuredCategory);
  apiRoute.route('/categories/detail').get(categoryController.detail);
};