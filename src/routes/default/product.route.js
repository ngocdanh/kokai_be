const productController = require('../../controllers/product.controller');
const { createProductValidator } = require('../../validators/product.validator');

module.exports = (apiRoute) => {
  apiRoute.route('/products/create').post(createProductValidator, productController.create);
  apiRoute.route('/products/list').get(productController.list);
  apiRoute.route('/products/detail').get(productController.detail);
  apiRoute.route('/products/update-all').put(productController.updateAll);
  apiRoute.route('/products/popular').get(productController.listProductPopular);
  apiRoute.route('/products/best-seller').get(productController.listProductBestSeller);
  apiRoute.route('/products/sale-of-day').get(productController.listDealOfDay);
};