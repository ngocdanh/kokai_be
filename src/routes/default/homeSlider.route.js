const homeSliderControler = require('../../controllers/homeSlider.controller');

module.exports = (apiRoute) => {
  apiRoute.route('/home-slider').get(homeSliderControler.list);
};