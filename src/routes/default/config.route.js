const configController = require('../../controllers/config.controller');

module.exports = (apiRoute) => {
  apiRoute.route('/configs/menu-list').get(configController.menuList);
};