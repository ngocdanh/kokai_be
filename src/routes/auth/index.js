const express = require('express');
const { verifyToken } = require('../../controllers/common.controller');

const authRouter = express.Router();

authRouter.use(async (req, res, next) => {
  await verifyToken(req, res, next);
});

require('./whislist.route')(authRouter);
module.exports = authRouter;
