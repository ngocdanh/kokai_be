const wishlistController = require('../../controllers/wishlist.controller');
const { 
  wishlistValidator, 
  deleteWishlistValidator,
  addWishlistValidator 
} = require('../../validators/wishlist.validator');

module.exports = (apiRoute) => {
  apiRoute.route('/wishlist/detail').get(wishlistController.detail);
  apiRoute.route('/wishlist/delete').post(deleteWishlistValidator, wishlistController.remove);
  apiRoute.route('/wishlist/add').post(addWishlistValidator, wishlistController.add);
  apiRoute.route('/wishlist/list').get(wishlistController.list);
  apiRoute.route('/wishlist/update').post( wishlistValidator,wishlistController.update);
};