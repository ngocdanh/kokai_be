const { CODES_SUCCESS, CODES_ERROR } = require('./messages');
const {default: mongoose} = require('mongoose');
const { isEmpty, uniqBy, isArray, unionBy, union } = require('lodash');
const moment = require('moment');
const bcrypt = require('bcryptjs');

const ObjectId = mongoose.Types.ObjectId;

const responseError = (statusCode, error = {}) => {
  const response = {};
  response.success = false;
  response.statusCode = statusCode;
  response.message = error?.message ?? CODES_ERROR[statusCode];
  response.error = error;
  if(isArray(error)) {
    response.error = unionBy(error, [], o => o?.msg);
  }
  if(error instanceof ReferenceError) {
    response.message = error?.message;
    response.errors = error?.stack;
  }
  if(error?.name === 'ValidationError') {
    const errors = {};

    Object.keys(error.errors).forEach((key) => {
      errors[key] = error.errors[key];
    });
    response.message = error?._message;
    response.errors = error?.errors;
  }
  return response;
};
const responseSuccess = (statusCode, result = {}) => {
  const response = {
    success: true,
    statusCode: statusCode,
    message: CODES_SUCCESS[statusCode],
  };
  if (result) {
    response.data = result;
  }
  return response;
};

const resJsonError = (res, err) => {
  return res.json(responseError(400001, err));
};

const generatorTime = () => moment().format('YYYY-MM-DD HH:mm:ss');
const connectDatabase = (databaseName) => {
  const conn = mongoose.dbs[databaseName];
  return conn;
};
const commonModel = {
  createdAt: { type: String, required: true, default: generatorTime },
  createdBy: { type: ObjectId, ref: 'users'},
  updatedAt: { type: String},
  updatedBy: {type: ObjectId, ref: 'users'},
};

const promiseResolve = (data) => Promise.resolve(data);
const promiseReject = (err) => Promise.reject(err);
const escapeRegExp = (string = '') => String(string).replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
const regExpSearch = (string = '') => {
  const regex = { $regex: escapeRegExp(string), $options: 'i'};
  return regex;
};
const convertToObjectId = (value) => (value ? ObjectId(value) : null);

const generateHashPassword = (password) => {
  const hash = bcrypt.hashSync(password, 8);
  return hash;
};
const convertStringToUTF8 = (value) => Buffer.from(value, 'utf-8').toString();
const generatorKey = (length) => {
  let result = '';
  const characters = 'ABCDEFGHIJ0123456789KLMNOPQRSTUVWXYZ0123456789abcdefghijklmn0123456789opqrstuvwxyz';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

const populateModel = (path, select, match = {}, option = {}) => {
  const populate = {
    path: path,
    select: select,
    match: match,
  };
  if (!isEmpty(option)) {
    populate.options = {...option };
  }
  return populate;
};

const colorize = (...args) => ({
  black: `\x1b[30m${args.join(' ')}`,
  red: `\x1b[31m${args.join(' ')}`,
  green: `\x1b[32m${args.join(' ')}`,
  yellow: `\x1b[33m${args.join(' ')}`,
  blue: `\x1b[34m${args.join(' ')}`,
  magenta: `\x1b[35m${args.join(' ')}`,
  cyan: `\x1b[36m${args.join(' ')}`,
  white: `\x1b[37m${args.join(' ')}`,
  bgBlack: `\x1b[40m${args.join(' ')}\x1b[0m`,
  bgRed: `\x1b[41m${args.join(' ')}\x1b[0m`,
  bgGreen: `\x1b[42m${args.join(' ')}\x1b[0m`,
  bgYellow: `\x1b[43m${args.join(' ')}\x1b[0m`,
  bgBlue: `\x1b[44m${args.join(' ')}\x1b[0m`,
  bgMagenta: `\x1b[45m${args.join(' ')}\x1b[0m`,
  bgCyan: `\x1b[46m${args.join(' ')}\x1b[0m`,
  bgWhite: `\x1b[47m${args.join(' ')}\x1b[0m`
});

module.exports = {
  responseError,
  responseSuccess,
  resJsonError,
  commonModel,
  generatorTime,
  connectDatabase,
  promiseResolve,
  promiseReject,
  regExpSearch,
  convertToObjectId,
  generateHashPassword,
  convertStringToUTF8,
  generatorKey,
  populateModel,
  colorize
};