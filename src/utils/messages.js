exports.CODES_SUCCESS = {
  /* Login start from 1001 to 1100 */
  10001: 'Đăng nhập thành công', 
  /* Product start from 1101 to 1200*/
  1101: 'Tạo sản phẩm thành công',
  1102: 'Lấy danh sách sản phẩm thành công',
  1103: 'Lấy thông tin sản phẩm thành công',
  1104: 'Cập nhật tất cả sản phẩm thành công',
  1105: 'Lấy danh sách sản phẩm nổi bật thành công',
  1106: 'Lấy danh sách sản phẩm chạy nhất thành công',
  1107: 'Lấy danh sách sản phẩm giảm giá trong ngày thành công',
  /* Menu start from 1201 to 1300 */
  1201: 'Lấy danh sách menu thành công',
  1202: 'Lấy danh sách menu header thành công',
  1203: 'Lấy danh sách menu footer thành công',
  1204: 'Lấy danh sách menu category thành công',

  /* Category start from 1301 to 1400 */
  1301: 'Lấy danh sách danh mục thành công',
  1302: 'Lấy chi tiết danh mục thành công',
  1303: 'Lấy danh sách danh mục nổi bật thành công',

  /* Home slider start from 1401 to 1500 */
  1401: 'Lấy danh sách banner trang chủ thành công',

  /* Login/Register start from 1501 to 1600 */
  1501: 'Tạo tài khoản thành công',
  1502: 'Đăng nhập tài khoản thành công',
  1503: 'Lấy mã token thành công',
  1504: 'Lấy mã token mới thành công',

  /* wishlist start from 1501 to 1600 */
  1601: 'Thêm vào danh sách yêu thích thành công',
  1602: 'Cập nhật danh sách yêu thích thành công',
  1603: 'Lấy danh sách yêu thích thành công',
  1604: 'Lấy danh sách yêu thích tất cả tài khoản thành công',
  1605: 'Xóa sản phẩm yêu thích thành công',
  1606: 'Thêm sản phẩm yêu thích thành công',
};
exports.CODES_ERROR = {
  /* Login failed start from 4000 to 4100 */
  4000: 'Đăng nhập thất bại',
  /* Product failed start from 41001 to 4200*/
  4101: 'Tạo sản phẩm thất bại',
  4102: 'Lấy danh sách sản phẩm thất bại',
  4103: 'Lấy thông tin sản phẩm thất bại',
  4104: 'Cập nhật tất cả sản phẩm thất bại',
  1105: 'Lấy danh sách sản phẩm nổi bật thất bại',
  1106: 'Lấy danh sách sản phẩm chạy nhất thất bại',
  1107: 'Lấy danh sách sản phẩm giảm giá trong ngày thất bại',
  /* Menu failed from 4201 to 4300 */
  4201: 'Lấy danh sách menu thất bại',
  4202: 'Lấy danh sách menu header thất bại',
  4203: 'Lấy danh sách menu footer thất bại',
  4204: 'Lấy danh sách menu category thất bại',

  /* Category start from 4301 to 4400 */
  4301: 'Lấy danh sách chủ đề thất bại',
  4302: 'Lấy thông tin chủ đề thất bại',
  4303: 'Lấy danh sách danh mục nổi bật thất bại',

  /* Home slider start from 4401 to 4500 */
  4401: 'Lấy danh sách banner trang chủ thất bại',

  /* Login/Register start from 4501 to 4600 */
  4501: 'Tạo tài khoản thất bại',
  4502: 'Đăng nhập tài khoản thất bại',
  4503: 'Lấy mã token thất bại',
  4504: 'Lấy mã token mới thất bại',
  4551: 'Email đã tồn tại',
  4552: 'Số điện thoại đã tồn tại',
  4553: 'Mã token không được để trống',
  4554: 'Mã token không đúng',
  4555: 'Mật khẩu không đúng',
  4556: 'Email không tồn tại',
  4557: 'Mã refresh token không đúng',

  /* wishlist start from 4601 to 4700 */
  4601: 'Thêm vào danh sách yêu thích thất bại',
  4602: 'Cập nhật danh sách yêu thích thất bại',
  4603: 'Lấy danh sách yêu thích thất bại',
  4604: 'Lấy danh sách yêu thích tất cả tài khoản thất bại',
  4605: 'Xóa sản phẩm yêu thích thất bại',
  1606: 'Thêm sản phẩm yêu thích thất bại',

  400001: 'Error catch!',
};