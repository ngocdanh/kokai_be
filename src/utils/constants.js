
exports.GET = 'get';
exports.POST = 'post';
exports.PUT = 'put';
exports.DELETE = 'delete';
exports.OPTIONS = 'options';

exports.MODEL = {
  products: 'products',
  configs: 'configs',
  categories: 'categories',
  featuredKeywords: 'featured_Keywords',
  homeSlider: 'home_sliders',
  accounts: 'accounts',
  wishlist: 'wishlist',
  carts: 'carts',
  blogs: 'blogs',
  vendors: 'vendors',
};

exports.ACCOUNT_TYPE = {
  user: 'user',
  admin: 'user',
  shop: 'shop',
};

exports.PROMOTION_CODES = ['SALE_OF_DAY', 'DEFAULT', 'HOT'];

exports.myCustomLabels = {
  totalDocs: 'itemCount',
  docs: 'items',
  limit: 'limit',
  page: 'currentPage',
  nextPage: 'nextPage',
  prevPage: 'prevPage',
  totalPages: 'pageCount',
  pagingCounter: 'pagingCounter',
  meta: 'paginator',
};

exports.EXPIRE_TIME_TOKEN = '2h';
exports.EXPIRE_TIME_REFRESH_TOKEN = '30d';