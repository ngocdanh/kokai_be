/**
Mr : Dang Xuan Truong
Email: truongdx@runsystem.net
 */
const axios = require('axios');
const btoaCredential = require('btoa');

const credentials = btoaCredential(`${process.env.NAME}:${process.env.PASS}`);

const sendQueryToAPI = async (data) => {
  try {
    const uri = data.uri;
    const xAccessToken = data.accessToken ? data.accessToken : '';
    const basicAuth = `Basic ${credentials}`;
    let result;
    if (xAccessToken) {
      const headerConfig = {
        timeout: 1000,
        headers: {
          Authorization: basicAuth,
          'x-access-token': xAccessToken,
        },
      };
      result = await axios.get(uri, headerConfig);
    } else {
      result = await axios.get(uri);
    }
    return Promise.resolve(result.data ? result.data : null);
  } catch (error) {
    return Promise.reject(error);
  }
};
module.exports = {
  sendQueryToAPI,
};
