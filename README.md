## Dự án KOKAI - BE

Được xây dựng từ Node js(Express js) + MongoDB(mongoose)

## Bắt đầu

Đầu tiên, chạy lệnh sau

```bash
npm install
copy .env.default to mkdir .env
npm run dev
# or
yarn dev
```

## Thành viên tham gia: 
- Ngọc Danh (danhnotes@gmail.com) - Team Lead + FE + BE
- Ngô Sỹ Dương () - FE + BE
- Phan Công Long () - FE + BE
- Văn Minh Trường (vanminhtruong678@gmail.com) - FE
- Lê Mai Linh () - FE
- Tiến () - FE

## Thông tin dự án:
Dự án buid bằng React js (Next js) + Node js(Express js) + noSQL(MongoDB)
- Clone theme: https://nest-nextjs.vercel.app
- Tìm icon SVG: https://www.flaticon.com/uicons
- Tailwind css cheat sheets: https://tailwindcomponents.com/cheatsheet/
- Trang chủ MUI: https://mui.com 