const express = require('express');
const events = require('events');
const bodyParser = require('body-parser');
const session = require('express-session');
const cors = require('cors');
const dotenv = require('dotenv');
const path = require('path');

const app = express();

dotenv.config();
app.use(require('morgan')('dev'));

app.use(require('method-override')());
app.use(express.static(__dirname + '/public'));

app.use(session({ secret: 'conduit', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false  }));
app.use(cors());

app.use(express.static(path.join(__dirname, 'public')));

/* Config */
app.use(bodyParser.json({limit: '50mb'})); 
app.use(bodyParser.urlencoded({ 
  limit: '50mb',
  extended: true,
}));
// require('./src/configs/configInit')(app);
require('./src/configs/database');
require('./src/routes/default')(app);
const authRoute = require('./src/routes/auth');
const { colorize } = require('./src/utils/shared');
app.use('/auth', authRoute);
require('./src/configs/configError')(app);


events.EventEmitter.defaultMaxListeners = 100000;
// finally, let's start our server...
const server = app.listen( process.env.PORT || 3000, function(){
  console.log(colorize('Listening on port ' + server.address().port).bgBlue);
});
